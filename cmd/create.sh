cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    orig_cmd_create \
	--hostname=$DOMAIN \
        "$@"    # accept additional 'docker create' options
}

_backup_cmd_create() {
    if [[ ! -d data/ ]]; then
        mkdir -p data/{var-lib-mysql,var-lib-imap,var-spool-imap,var-lib-dirsrv,etc-dirsrv,etc-kolab}
        orig_cmd_create --hostname=$DOMAIN "$@"
        docker cp -a $CONTAINER:/var/lib/mysql/. data/var-lib-mysql/
        docker cp -a $CONTAINER:/var/lib/imap/. data/var-lib-imap/
        docker cp -a $CONTAINER:/var/spool/imap/. data/var-spool-imap/
        docker cp -a $CONTAINER:/var/lib/dirsrv/. data/var-lib-dirsrv/
        docker cp -a $CONTAINER:/etc/dirsrv/. data/etc-dirsrv/
        docker cp -a $CONTAINER:/etc/kolab/. data/etc-kolab/
    fi
    orig_cmd_create \
        --hostname=$DOMAIN \
        --mount type=bind,src="$(pwd)/data/var-lib-mysql/",dst=/var/lib/mysql/ \
        --mount type=bind,src="$(pwd)/data/var-lib-imap/",dst=/var/lib/imap/ \
        --mount type=bind,src="$(pwd)/data/var-spool-imap/",dst=/var/spool/imap/ \
        --mount type=bind,src="$(pwd)/data/var-lib-dirsrv/",dst=/var/lib/dirsrv/ \
        --mount type=bind,src="$(pwd)/data/etc-dirsrv/",dst=/etc/dirsrv/ \
        --mount type=bind,src="$(pwd)/data/etc-kolab/",dst=/etc/kolab/ \
        "$@"    # accept additional 'docker create' options
}
