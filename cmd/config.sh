cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    # check settings
    if [[ $DOMAIN != 'kolab.example.org' ]] && [[ $PASS == 'Pass123!' ]]; then
	    echo "Error: PASS on 'settings.sh' has to be changed for security reasons." >&2
        exit 1
    fi

    ds inject debian-fixes.sh
    ds inject setup-apache2.sh
    #ds inject setup-kolab.sh
    #ds inject setup.sh
}
