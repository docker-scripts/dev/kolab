cmd_backup_help() {
    cat <<_EOF
    backup
        Backup the configuration and data of the container.

_EOF
}

cmd_backup() {
    set -x
    # create the backup dir
    local backup="backup-$(date +%Y%m%d)"
    rm -rf $backup
    rm -f $backup.tgz
    mkdir $backup

    # copy the data to the backup dir
    ds inject backup.sh $backup

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$backup.tgz $backup/
    rm -rf $backup/
}
