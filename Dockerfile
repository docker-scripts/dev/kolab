include(stretch)

RUN apt update &&\
    apt upgrade --yes

RUN apt install --yes wget apt-transport-https gnupg &&\
    wget -q -O- https://ssl.kolabsys.com/community.asc | apt-key add -

RUN echo "\
deb http://obs.kolabsys.com/repositories/Kolab:/16/Debian_9.0/ ./\n\
deb-src http://obs.kolabsys.com/repositories/Kolab:/16/Debian_9.0/ ./\n\
" > /etc/apt/sources.list.d/kolab.list

RUN echo "\
Package: *\n\
Pin: origin obs.kolabsys.com\n\
Pin-Priority: 501\n\
" > /etc/apt/preferences.d/kolab

RUN apt update
RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes kolab

### install other tools
RUN apt install --yes vim git unzip
