APP=kolab
PORTS="25:25 110:110 143:143 389:389 465:465 587:587
       636:636 993:993 995:995 4190:4190 8080:8080"
DOMAIN="kolab.example.org"

### Password of the admin user 'cn=Directory Manager'
### at https://kolab.example.org/kolab-webadmin/
PASS=Pass123!
