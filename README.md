# Kolab in a container

https://kolab.org/

## Installation

  - First install `ds` and `wsproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/wsproxy#installation

  - Then get the scripts: `ds pull kolab`

  - Create a directory for the container: `ds init kolab @kolab.example.org`

  - Fix the settings: `cd /var/ds/kolab.example.org/ ; vim settings.sh`

  - Make the container: `ds make`

  - If the domain is not a real one (for example if installed on a local machine),
    add to `/etc/hosts` the line `127.0.0.1 kolab.example.org`.
    Then open in browser: https://kolab.example.org

## Other commands

```
ds stop
ds start
ds shell
ds help
```

## Some other docs

- Kolab documentation: https://docs.kolab.org/
- DNS configuration for a mail server:
  https://gitlab.com/docker-scripts/postfix/blob/master/INSTALL.md#3-minimal-dns-configuration
