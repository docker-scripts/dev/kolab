#!/bin/bash -x
### See: https://docs.kolab.org/administrator-guide/backup-and-restore.html

backup() {
    # go to the backup dir
    backup=$1
    cd /host/$backup

    backup_directory_server
    backup_imap
    backup_mysql

    cp /etc/kolab/kolab.conf .
    cp /host/settings.sh .

    # custom backup script
    [[ -f /host/backup.sh ]] && source /host/backup.sh

    # backup any custom scripts
    backup_custom_scripts
}

backup_directory_server() {
    dir_list=$(find /etc/dirsrv/ -mindepth 1 -maxdepth 1 -type d -name "slapd-*" | xargs -n 1 basename)
    for dir in $dir_list; do
        db_list=$(find /var/lib/dirsrv/$dir/db/ -mindepth 1 -maxdepth 1 -type d | xargs -n 1 basename)
        for nsdb in $db_list; do
            fname=$(hostname)-${dir#slapd-}-$nsdb.ldif
            ns-slapd db2ldif -D /etc/dirsrv/$dir -n $nsdb -a /tmp/$fname &>/dev/null
            mv /tmp/$fname .
        done
    done

    mkdir -p etc-dirsrv
    cp -a /etc/dirsrv/* etc-dirsrv/
}

backup_imap() {
    mkdir -p var-spool-imap
    cp -a /var/spool/imap/* var-spool-imap/
    rm -rf var-spool-imap/{stage.,sync.}
    for file in cyrus.squat cyrus.cache.NEW cyrus.expunge.NEW cyrus.index.NEW; do
        find var-spool-imap -name "$file" -delete
    done

    mkdir -p var-lib-imap
    cp -a /var/lib/imap/* var-lib-imap/
    rm -rf var-lib-imap/socket/
    find var-lib-imap/ -type f -name '*.lock*' -delete
}

backup_mysql() {
    mysqldump --databases kolab roundcube > dump.sql
}

backup_custom_scripts() {
    [[ -f /host/backup.sh ]] && cp /host/backup.sh .
    [[ -f /host/restore.sh ]] && cp /host/restore.sh .
    [[ -d /host/cmd ]] && cp -a /host/cmd .
    [[ -d /host/scripts ]] && cp -a /host/scripts .
}

# start backup
backup "$@"
