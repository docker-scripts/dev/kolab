#!/bin/bash -x

# enable mod ssl of apache2
a2enmod ssl
a2dissite 000-default
a2ensite default-ssl
systemctl restart apache2
