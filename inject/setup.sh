#!/bin/bash -x

# mysql
chown -R mysql:mysql /var/lib/mysql
[[ -d /var/lib/mysql/mysql ]] || mysql_install_db

# imap
chown cyrus:mail -R /var/lib/imap/
chown cyrus:mail -R /var/spool/imap/

# dirsrv
chown dirsrv:dirsrv -R /etc/dirsrv/
chown dirsrv:dirsrv -R /var/lib/dirsrv/

# kolab
chown www-data:www-data -R /etc/kolab/
