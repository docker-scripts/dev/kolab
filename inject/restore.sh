#!/bin/bash -x

restore() {
    # go to the backup directory
    backup=$1
    cd /host/$backup

    restore_directory_server
    restore_imap
    restore_mysql
    #restore_kolab    
    restore_custom_scripts

    # custom restore script
    [[ -f /host/restore.sh ]] && source /host/restore.sh
}

restore_directory_server() {
    systemctl stop dirsrv.target
    sleep 3

    rm -rf /etc/dirsrv
    mkdir -p /etc/dirsrv
    cp -a etc-dirsrv/* /etc/dirsrv/
    chown dirsrv:dirsrv -R /etc/dirsrv/

    for ldif_file in *.ldif; do
        name=${ldif_file%.ldif}
        instance=$(echo $name | cut -d- -f2)
        database=$(echo $name | cut -d- -f3)
        cp $ldif_file /tmp/
        chmod 666 /tmp/$ldif_file
        ns-slapd ldif2db \
                 -D /etc/dirsrv/slapd-$instance \
                 -n $database \
                 -i $(pwd)/$ldif_file
        rm /tmp/$ldif_file
    done
    
    systemctl start dirsrv.target
}

restore_imap() {
    rm -rf /var/spool/imap
    mkdir -p /var/spool/imap
    cp -a var-spool-imap/* /var/spool/imap/
    chown cyrus:mail -R /var/spool/imap

    rm -rf /var/lib/imap
    mkdir -p /var/lib/imap
    cp -a var-lib-imap/* /var/lib/imap/
    chown cyrus:mail -R /var/lib/imap
}

restore_mysql() {
    mysql -e "DROP DATABASE kolab; DROP DATABASE roundcube;"
    mysql < dump.sql
}

restore_kolab() {
    cp kolab.conf /etc/kolab/
    chown www-data:www-data /etc/kolab/kolab.conf
}

restore_custom_scripts() {
    if [[ ! -f /host/backup.sh ]] && [[ -f backup.sh ]]; then
        cp backup.sh /host/
    fi
    if [[ ! -f /host/restore.sh ]] && [[ -f restore.sh ]]; then
        cp restore.sh /host/
    fi
    if [[ ! -d /host/cmd ]] && [[ -d cmd ]]; then
        cp -a cmd /host/
    fi
    if [[ ! -d /host/scripts ]] && [[ -d scripts ]]; then
        cp -a scripts /host/
    fi
}

# start restore
restore "$@"
