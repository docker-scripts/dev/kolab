#!/bin/bash -x

# setup kolab
source /host/settings.sh  # get PASS
setup-kolab --directory-manager-pwd=$PASS \
            --default --yes --mysqlserver=existing

# enable dirsrv
systemctl enable dirsrv.target
